# SPDX-FileCopyrightText: 2022 Emma Turner <em.turner@tutanota.com>
#
# SPDX-License-Identifier: MIT

tinywl: tinywl/core
	cd tinywl/core && $(MAKE)

clock-time: emturner/clock.c
	$(CC) $(CFLAGS) \
        -shared \
		-g -Werror -I/gnu/store/1jgcbdzx2ss6xv59w55g3kr3x4935dfb-guile-3.0.8/include/guile/3.0 \
		-DWLR_USE_UNSTABLE \
		-o $@.so -fPIC $< \
		$(LIBS)

deps: tinywl/core.scm wayland/server-core.scm wayland/server-protocol.scm \
		emturner/util.scm \
		wlr/dylib.scm wayland/util.scm \
		wlr/types/wlr-output.scm \
        clock-time emturner/clock.scm \
        tinywl tinywl/wrapper.scm

check: deps
	GUILE_EXTENSIONS_PATH=$(GUIX_ENVIRONMENT)/lib:$(PWD):$(GUILE_EXTENSIONS_PATH) \
		guile -L . -c '(use-modules (tinywl core)) (check)'

prompt: deps
	GUILE_EXTENSIONS_PATH=$(GUIX_ENVIRONMENT)/lib:$(PWD):$(GUILE_EXTENSIONS_PATH) \
		guile -L .

clean:
	rm -f tinywl.so xdg-shell-protocol.h xdg-shell-protocol.c

.DEFAULT_GOAL=check
.PHONY: clean

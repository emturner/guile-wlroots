; SPDX-FileCopyrightText: 2022 Emma Turner <em.turner@tutanota.com>
;
; SPDX-License-Identifier: MIT

(define-module (tinywl core)
  #:use-module (system foreign)
  #:use-module (system foreign-library)
  #:use-module (rnrs enums)
  #:use-module (srfi srfi-9)
  #:use-module (ice-9 format)
  #:use-module (ice-9 match)
  #:use-module (oop goops)
  #:use-module (wayland server-core)
  #:use-module (wlr dylib)
  #:use-module (wlr backend)
  #:use-module (wlr render wlr-renderer)
  #:use-module (wlr util log)
  #:use-module (wayland util)
  #:use-module (wlr types wlr-output)
  #:use-module (emturner util)
  #:use-module (emturner clock)
  #:use-module (tinywl wrapper)
  #:use-module (ice-9 optargs)
  #:export (run check))

;; -------------
;; tinywl.c port
;; -------------
(define tinywl-cursor-mode
  (make-enumeration '(tinywl-cursor-passthrough
                      tinywl-cursor-move
                      tinywl-cursor-resize)))

(define-class <tinywl-server> ()
  ;; struct wl_display *wl_display;
  (display #:init-value %null-pointer
           #:accessor tinywl-server->display)
  ;; struct wlr_backend *backend;
  (backend #:init-value %null-pointer
           #:accessor tinywl-server->backend)
  ;; struct wlr_renderer *renderer;
  (renderer #:init-value %null-pointer
            #:accessor tinywl-server->renderer)
  ;; struct wlr_xdg_shell *xdg_shell;
  (xdg-shell #:init-value %null-pointer
             #:accessor tinywl-server->xdg-shell)
  ;; struct wl_listener new_xdg_surface;
  (new-xdg-surface #:init-value (make <wl-listener>)
                   #:accessor tinywl-server->new-xdg-surface)
  ;; struct wl_list views; FIXME views subclass
  (views #:init-value (make <wl-list>)
         #:accessor tinywl-server->views)
  ;; struct wlr_cursor *cursor;
  (cursor #:init-value %null-pointer
          #:accessor tinywl-server->cursor)
  ;; struct wlr_xcursor_manager *cursor_mgr;
  (xcursor-mgr #:init-value %null-pointer
               #:accessor tinywl-server->xcursor-mgr)
  ;; struct wl_listener cursor_motion;
  (cursor-motion #:init-value (make <wl-listener>)
                 #:accessor tinywl-server->cursor-motion)
  ;; struct wl_listener cursor_motion_absolute;
  (cursor-motion-absolute #:init-value (make <wl-listener>)
                          #:accessor tinywl-server->cursor-motion-absolute)
  ;; struct wl_listener cursor_button;
  (cursor-button #:init-value (make <wl-listener>)
                 #:accessor tinywl-server->cursor-button)
  ;; struct wl_listener cursor_axis;
  (cursor-axis #:init-value (make <wl-listener>)
               #:accessor tinywl-server->cursor-axis)
  ;; struct wl_listener cursor_frame;
  (cursor-frame #:init-value (make <wl-listener>)
                #:accessor tinywl-server->cursor-frame)
  ;; struct wlr_seat *seat;
  (seat #:init-value %null-pointer
        #:accessor tinywl-server->seat)
  ;; struct wl_listener new_input;
  (new-input #:init-value (make <wl-listener>)
             #:accessor tinywl-server->new-input)
  ;; struct wl_listener request_cursor;
  (request-cursor #:init-value (make <wl-listener>)
                  #:accessor tinywl-server->request-cursor)
  ;; struct wl_listener request_set_selection;
  (request-set-selection #:init-value (make <wl-listener>)
                         #:accessor tinywl-server->request_set-selection)
  ;; struct wl_list keyboards; FIXME keyboard subclass
  (keyboards #:init-value (make <wl-list>)
             #:accessor tinywl-server->keyboards)
  ;; enum tinywl_cursor_mode cursor_mode;
  (cursor-mode #:init-value 'tinywl-cursor-passthrough
               #:accessor tinywl-server->cursor-mode)
  ;; struct tinywl_view *grabbed_view;
  (grabbed-view #:init-value %null-pointer
                #:accessor tinywl-server->grabbed-view)
  ;; double grab_x, grab_y;
  (grab-coords #:init-value '(0.0 . 0.0)
               #:accessor tinywl-server->grab-coords)
  ;; struct wlr_box grab_geobox; FIXME pointer
  (grab-geobox #:init-value %null-pointer
               #:accessor tinywl-server->grab-geobox)
  ;; uint32_t resize_edges;
  (resize-edges #:init-value 0
                #:accessor tinywl-server->resize-edges)
  ;; struct wlr_output_layout *output_layout;
  (output-layout #:init-value %null-pointer
                 #:accessor tinywl-server->output-layout)
  ;; struct wl_list outputs; FIXME output subclass
  (outputs #:init-value (make <wl-list>)
           #:accessor tinywl-server->outputs)
  ;; struct wl_listener new_output;
  (new-output #:init-value (make <wl-listener>)
              #:accessor tinywl-server->new-output))

(define-class <tinywl-output> (<wl-list>)
  ;; struct tinywl_server *server;
  (server #:getter tinywl-output->server
          #:init-keyword #:server)
  ;; struct wlr_output *wlr_output;
  (output #:getter tinywl-output->wlr-output
          #:init-keyword #:output)
  ;; struct wl_listener frame;
  (listener #:init-value (make <wl-listener>)
            #:getter tinywl-output->frame
            #:init-keyword #:frame))

;; (define (output-frame tinywl-output)
;;   "This function is called every time an output is ready to display a frame,
;; generally at the output's refresh rate (e.g. 60Hz)."
;;   (proc->wl-notify-func-t
;;    (lambda (listener-raw-ptr data-raw-ptr)
;;      (let ((server (tinywl-output->server tinywl-output))
;;            (renderer (tinywl-server->renderer))
;;            (now (clock-gettime->monotomic)))
;;        ;; /* wlr_output_attach_render makes the OpenGL context current. */
;;        (if (attach-render (tinywl-output->wlr-output tinywl-output))
;;            (let ((res (effective-resolution tinywl-output->wlr-output tinywl-output)))
;;              #f))))))


(define (server-new-output-notify server)
  "This event is raised by the backend when a new output (aka a display
or a monitor) becomes available."
  (proc->wl-notify-func-t
   (lambda (listener-raw-ptr data-raw-ptr)
    (let ((wlr-output (make <wlr-output>
                        #:ptr (wrap-wlr-output-ptr data-raw-ptr))))
      ;; Some backends don't have modes. DRM+KMS does,
      ;; and we need to set a mode before we can use the output.
      ;; The mode is a tuple of (width, height, refresh rate), and
      ;; each monitor supports only a specific set of modes. We just
      ;; pick the monitor's preferred mode, a more sophisticated
      ;; compositor would let the user configure it.
      (and (set-preferred-mode wlr-output)
           ;; Allocates and configures our state for this output
           (make <tinywl-output>
             #:output wlr-output
             #:server server)))))) ;; FIXME: frame callback: static void output_frame

;; (define (run verbosity)
;;   (wlr-log-init verbosity)
;;   ;; TODO: startup command
;;   (define server (make <tinywl-server>))
;;   ;; The wayland display is managed by libwayland. It handles accepting
;;   ;; clients from the Unix socket, managing Wayland globals, and so on.
;;   (set! (tinywl-server->display server) (wl-display-create))
;;   ;; The backend is a wlroots feature which abstracts the underlying input
;;   ;; and output hardware. The autocreate option will choose the most
;;   ;; suitable backend based on the current environment, such as opening
;;   ;; an X11 window if an X11 server is running.
;;   (set! (tinywl-server->backend server)
;;         (wlr-backend-auto-create (tinywl-server->display server)))
;;   ;; If we don't provide a renderer, autocreate makes a GLES2 renderr for
;;   ;; us.  The renderer is responsible for defining the various pixel
;;   ;; formats it supports for shared memory, this configures that for
;;   ;; clients.
;;   (set! (tinywl-server->renderer server)
;;         (wlr-renderer-auto-create (tinywl-server->backend server)))
;;   (or (wlr-renderer-init-wl-display
;;        (tinywl-server->renderer server)
;;        (tinywl-server->display server))
;;       (throw "failed to initialise wl-display"))
;;   (wlr-compositor-create (tinywl-server->display server)
;;                          (tinywl-server->renderer server))
;;   (wlr-data-device-manager-create (tinywl-server->display server))
;;   (set! (tinywl-server->output-layout server)
;;         (wlr-output-layout-create))

;;   ;; TODO Configure a listener to be notified when new outputs are available
;;   ;; on the backend.
;;   ;; (server_new_output_notify ('(todo func)))
;;   server)

;; (define (check)
;;   (run 'wlr-error))

(define (make-handle-keybinding server)
  (procedure->pointer
   cstdbool
   (lambda (sym)
     "Here we handle compositor keybindings. This is when the compositor is processing keys,
rather than passing them on to the client for its own processing.

This function assumes Alt is held down."
     (let ((result (match sym
                     ;; XKB_KEY_Escape
                     (#xff1b
                      (or (wl-display-terminate (server->wl-display server))
                          #t))
                     ;; XKB_KEY_F1
                     (#xffbe
                      (tinywl-cycle-focus-next-view server))
                     ;; default
                     (_ #f))))
       (bool->cstdbool result)))
   `(,uint32)))

(define* (gwwm-run #:key (startup-cmd #f)
                         (log-level   'wlr-log-error)
                         (handle-kb   make-handle-keybinding))
  (wlr-log-init log-level)
  (tinywl-run startup-cmd handle-kb))

(define (check)
  (gwwm-run #:startup-cmd "alacritty"
            #:log-level 'wlr-log-error))

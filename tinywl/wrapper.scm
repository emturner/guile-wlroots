; SPDX-FileCopyrightText: 2022 Emma Turner <em.turner@tutanota.com>
;
; SPDX-License-Identifier: MIT

(define-module (tinywl wrapper)
  #:use-module (ice-9 format)
  #:use-module (system foreign)
  #:use-module (oop goops)
  #:use-module (wayland server-core)
  #:use-module (wayland util)
  #:use-module (emturner util)
  #:export (tinywl-run
            handle-keybinding
            unwrap-tinywl-server
            server->wl-display
            tinywl-cycle-focus-next-view))

(load-extension "tinywl/core/tinywl" "init_tinywl_wrapper")

;; TODO tinywl_view struct - inherit from wl-list?
;; - need a <wl-list-entry> that takes a class that inherits from <wl-list>
;; - this <wl-list-entry> inherits from <wl-list>
;;   and allows access (next/prev) to an instance of the passed in class
;; - these passed in classes need to be able to construct themselves from
;;   a wl-list pointer (to give `lst' and `slf' pointers)
;; - wl-list-entry supports all wl-list methods except for wl-list-remove
(define-wrapped-pointer-type tinywl-view
  tinywl-view?
  wrap-tinywl-view unwrap-tinywl-view
  (lambda (view prt)
    (let ((v (unwrap-tinywl-view view)))
      (format prt "#<tinywl-view at ~x>"
              (pointer-address v)))))

(define-class <tinywl-view> (<wl-list-entry>))

(define-wrapped-pointer-type tinywl-server
  tinywl-server?
  wrap-tinywl-server unwrap-tinywl-server
  (lambda (wlr-b prt)
    (format prt "#<tinywl-server at ~x>"
        (pointer-address (unwrap-tinywl-server wlr-b)))))

(define (server->wl-display server)
  (let* ((svr (unwrap-tinywl-server server))
         (dsp (server-get-wl-display svr)))
    (wrap-wl-display dsp)))

;;(define (tinywl-cycle-focus-next-view server)
;;  (focus-next-view (unwrap-tinywl-server server)))

(define (wl-list-to-tinywl-view l)
  (wrap-tinywl-view
   (wl-list-get-tinywl-view
    (unwrap-wl-list
     (wl-list-inner l)))))

(define (tinywl-cycle-focus-next-view server)
  (let* ((views (server-get-views (unwrap-tinywl-server server)))
         (views (make <wl-list> #:wl-list-inner (wrap-wl-list views))))
    (if (< (wl-list-length views) 2)
        #t
        (let* ((curr-view (wl-list-next-entry
                           views
                           wl-list-to-tinywl-view))
               (next-view (wl-list-next-entry curr-view)))
          (focus-view
           (unwrap-tinywl-view (wl-list-entry-value next-view)))
          ;; Move previous view to end of list
          (wl-list-remove curr-view)
          (wl-list-insert (wl-list-prev views)
                          curr-view)))))

(define (tinywl-run startup-cmd handle-keybinding)
  (let* ((server (wrap-tinywl-server (init-server)))
         (handle-kb (handle-keybinding server)))
    (register-keybinding (unwrap-tinywl-server server) handle-kb)
    (run (unwrap-tinywl-server server) startup-cmd)))

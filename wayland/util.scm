; SPDX-FileCopyrightText: 2022 Emma Turner <em.turner@tutanota.com>
;
; SPDX-License-Identifier: MIT

;; Scheme wrapper for <wayland-util.h>
(define-module (wayland util)
  #:use-module (ice-9 format)
  #:use-module (ice-9 match)
  #:use-module (oop goops)
  #:use-module (system foreign)
  #:use-module (system foreign-library)

  #:use-module (wlr dylib)
  #:export (<wl-list>
            wl-list-init
            wl-list-inner
            wl-list-length
            wl-list-empty?
            wl-list-remove
            wl-list-insert
            wl-list-c-type
            wl-list
            wl-list?
            wl-list-next
            wl-list-prev
            wrap-wl-list
            unwrap-wl-list
            <wl-list-entry>
            wl-list-next-entry
            wl-list-entry-value))

;; struct wl_list {
;;   /** Previous list element */
;;   struct wl_list *prev;
;;   /** Next list element */
;;   struct wl_list *next;
;; };
(define wl-list-c-type '(* *))

(define-wrapped-pointer-type wl-list
  wl-list?
  wrap-wl-list unwrap-wl-list
  (lambda (lst prt)
    (let* ((unwrapped (unwrap-wl-list lst))
       (parsed (parse-c-struct unwrapped wl-list-c-type))
       (address (pointer-address unwrapped))
       (prev (pointer-address (car parsed)))
       (next (pointer-address (cadr parsed))))
      (format prt "#<wl-list at ~x | next ~x | prev ~x>"
          address
          next
          prev))))

(define (wl-list-ptrs l)
  (let* ((unwrapped (unwrap-wl-list l))
         (parsed (parse-c-struct unwrapped wl-list-c-type))
         (next (cadr parsed))
         (prev (car parsed)))
    `(,(wrap-wl-list next) .
      ,(wrap-wl-list prev))))

(define inner-init
  (let ((init (foreign-library-function wlroots "wl_list_init"
                    #:return-type void
                    #:arg-types '(*))))
    (lambda (lst)
      "Initialise the list"
      (init (unwrap-wl-list lst)))))

(define (make-wl-list)
  "Creates a new, initialised wl-list instance"
  (let ((lst (wrap-wl-list
          (make-c-struct wl-list-c-type
                         `(,%null-pointer ,%null-pointer)))))
    (inner-init lst)
    lst))

(define-class <wl-list> ()
  (lst #:init-value (make-wl-list)
       #:accessor wl-list-inner
       #:init-keyword #:wl-list-inner))

(define-class <wl-list-entry> (<wl-list>)
  (get-value #:accessor wl-list-entry-value-getter
             #:init-keyword #:value-getter))

(define-method (wl-list-next-entry (l <wl-list>) to-value)
  "Gets the next entry of the list"
  (let ((next (wl-list-next l)))
    (make <wl-list-entry>
      #:wl-list-inner (wl-list-inner next)
      #:value-getter to-value)))

(define-method (wl-list-next-entry (l <wl-list-entry>))
  (let ((next (wl-list-next l)))
    (make <wl-list-entry>
      #:wl-list-inner (wl-list-inner next)
      #:value-getter (wl-list-entry-value-getter l))))

(define-method (wl-list-entry-value (entry <wl-list-entry>))
  ((wl-list-entry-value-getter entry) entry))

(define inner-length
  (let ((len (foreign-library-function wlroots "wl_list_length"
                       #:return-type int
                       #:arg-types '(*))))
    (lambda (lst)
      (len (unwrap-wl-list lst)))))

(define-method (wl-list-init (l <wl-list>))
  "Initialises the inner list."
  (inner-init (wl-list-inner l)))

(define-method (wl-list-length (l <wl-list>))
  "Returns the length of the wl-list `lst`"
  (inner-length (wl-list-inner l)))

(define inner-empty?
  (let ((is-empty (foreign-library-function wlroots "wl_list_empty"
                        #:return-type int
                        #:arg-types '(*))))
    (lambda (lst)
      (is-empty (unwrap-wl-list lst)))))

(define-method (wl-list-empty? (l <wl-list>))
  "Returns #t if the list is empty, #f otherwise"
  (eq? 1
       (inner-empty?
    (wl-list-inner l))))

(define inner-remove
  (let ((remove (foreign-library-function wlroots "wl_list_remove"
                        #:return-type void
                        #:arg-types '(*))))
    (lambda (lst)
      (remove (unwrap-wl-list lst)))))

(define-method (wl-list-remove (l <wl-list>))
  "Removes the element l from the containing list, and reinitialises l."
  (inner-remove (wl-list-inner l))
  (inner-init (wl-list-inner l)))

(define inner-insert
  (let ((insert (foreign-library-function wlroots "wl_list_insert"
                                          #:return-type void
                                          #:arg-types '(* *))))
    (lambda (at link)
      (insert (unwrap-wl-list at)
              (unwrap-wl-list link)))))

(define-method (wl-list-insert (at <wl-list>) (link <wl-list>))
  (let* ((at (wl-list-inner at))
         (link (wl-list-inner link)))
    (inner-insert at link)))

(define-method (wl-list-next (l <wl-list>))
  "Get the next link in the list."
  (let* ((lst (wl-list-inner l))
         (ptrs (wl-list-ptrs lst)))
    (make <wl-list>
      #:wl-list-inner (car ptrs))))

(define-method (wl-list-prev (l <wl-list>))
  "Get the prev link in the list."
  (let* ((lst (wl-list-inner l))
         (ptrs (wl-list-ptrs lst)))
    (make <wl-list>
      #:wl-list-inner (cdr ptrs))))

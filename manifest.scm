; SPDX-FileCopyrightText: 2022 Emma Turner <em.turner@tutanota.com>
;
; SPDX-License-Identifier: MIT

(specifications->manifest
  '("wlroots"
    "wayland-protocols"
    "make"
    "glibc-locales"
    "gcc-toolchain"
    "pkg-config"
    "eudev"
    "libinput"
;;    "libseat"
    "libxkbcommon"
    "mesa"
    "pixman"
    "seatd"
    "wayland"
    "wayland-protocols"
    "xcb-util-errors"
    "xcb-util-wm"
    "xorg-server-xwayland"
;;    "elogind"

    ;; tinywl.scm
    "guile"

    ;; make check starts term
    "alacritty"
    "bash"
    "zsh"
    ))

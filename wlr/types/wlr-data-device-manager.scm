; SPDX-FileCopyrightText: 2022 Emma Turner <em.turner@tutanota.com>
;
; SPDX-License-Identifier: MIT

(define-module (wlr types wlr-data-device-manager)
  #:use-module (system foreign)
  #:use-module (system foreign-library)
  #:use-module (ice-9 format)
  #:use-module (wayland server-core)
  #:use-module (wlr dylib)
  #:use-module (wlr backend)
  #:use-module (emturner util)
  #:export-syntax (unwrap-wlr-renderer))

;; --------------------------------------------------
;; Wrappers for 'wlr/types/wlr_data_device_manager.h'
;; WARNING: use of unstable api
;; --------------------------------------------------
(define-wrapped-pointer-type wlr-data-device-manager
  wlr-data-device-manager?
  wrap-wlr-data-device-manager unwrap-wlr-data-device-manager
  (lambda (data-device-manager prt)
    (format prt "#<wlr-data-device-manager at ~x>"
        (pointer-address (unwrap-wlr-data-device-manager data-device-manager)))))

(define wlr-data-device-manager-create
  (let ((create
     (foreign-library-function wlroots "wlr_data_device_manager_create"
                   #:return-type '*
                   #:arg-types '(*))))
    (lambda (display)
      "Create the data-device-manager - handles the clipboard."
      (wrap-wlr-data-device-manager
       (create (unwrap-wl-display display))))))

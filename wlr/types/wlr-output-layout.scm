; SPDX-FileCopyrightText: 2022 Emma Turner <em.turner@tutanota.com>
;
; SPDX-License-Identifier: MIT

(define-module (wlr types wlr-output-layout)
  #:use-module (system foreign)
  #:use-module (system foreign-library)
  #:use-module (ice-9 format)
  #:use-module (wayland server-core)
  #:use-module (wlr dylib)
  #:use-module (wlr backend)
  #:use-module (emturner util)
  #:export-syntax (unwrap-wlr-renderer))

;; --------------------------------------------
;; Wrappers for 'wlr/types/wlr_output_layout.h'
;; WARNING: use of unstable api
;; --------------------------------------------
(define-wrapped-pointer-type wlr-output-layout
  wlr-output-layout?
  wrap-wlr-output-layout unwrap-wlr-output-layout
  (lambda (output-layout prt)
    (format prt "#<wlr-output-layout at ~x>"
        (pointer-address (unwrap-wlr-output-layout output-layout)))))

(define wlr-output-layout-create
  (let ((create
     (foreign-library-function wlroots "wlr_output_layout_create"
                   #:return-type '*
                   #:arg-types '())))
    (lambda ()
      "Create an output layout - utility for working with arrangement of
screens in a physical layout."
      (wrap-wlr-output-layout (create)))))

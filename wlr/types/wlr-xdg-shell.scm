; SPDX-FileCopyrightText: 2022 Emma Turner <em.turner@tutanota.com>
;
; SPDX-License-Identifier: MIT

(define-module (wlr types wlr-xdg-shell)
  #:use-module (system foreign)
  #:use-module (system foreign-library)
  #:use-module (ice-9 format)
  #:use-module (wayland server-core)
  #:use-module (wlr dylib)
  #:use-module (wlr backend)
  #:use-module (emturner util)
  #:export-syntax (unwrap-wlr-renderer))

;; ----------------------------------------
;; Wrappers for 'wlr/types/wlr_xdg_shell.h'
;; WARNING: use of unstable api
;; ----------------------------------------
(define-wrapped-pointer-type wlr-xdg-shell
  wlr-xdg-shell?
  wrap-wlr-xdg-shell unwrap-wlr-xdg-shell
  (lambda (xdg-shell prt)
    (format prt "#<wlr-xdg-shell at ~x>"
        (pointer-address (unwrap-wlr-xdg-shell xdg-shell)))))

(define wlr-xdg-shell-create
  (let ((create
     (foreign-library-function wlroots "wlr_xdg_shell_create"
                   #:return-type '*
                   #:arg-types '(*))))
    (lambda (display)
      "Initializes the xdg-shell"
      (wrap-wlr-xdg-shell
       (create (unwrap-wl-display display))))))

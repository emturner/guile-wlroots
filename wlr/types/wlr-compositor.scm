; SPDX-FileCopyrightText: 2022 Emma Turner <em.turner@tutanota.com>
;
; SPDX-License-Identifier: MIT

(define-module (wlr render wlr-compositor)
  #:use-module (system foreign)
  #:use-module (system foreign-library)
  #:use-module (ice-9 format)
  #:use-module (wayland server-core)
  #:use-module (wlr dylib)
  #:use-module (wlr backend)
  #:use-module (emturner util)
  #:export-syntax (unwrap-wlr-renderer))


;; -----------------------------------------
;; Wrappers for 'wlr/types/wlr_compositor.h'
;; WARNING: use of unstable api
;; -----------------------------------------
(define-wrapped-pointer-type wlr-compositor
  wlr-compositor?
  wrap-wlr-compositor unwrap-wlr-compositor
  (lambda (compositor prt)
    (format prt "#<wlr-compositor at ~x>"
        (pointer-address (unwrap-wlr-compositor compositor)))))

(define wlr-compositor-create
  (let ((create
     (foreign-library-function wlroots "wlr_compositor_create"
                   #:return-type '*
                   #:arg-types '(* *))))
    (lambda (display renderer)
      "Create the compositor - necessary for clients to allocate surfaces."
      (wrap-wlr-compositor
       (create (unwrap-wl-display display)
           (unwrap-wlr-renderer renderer))))))

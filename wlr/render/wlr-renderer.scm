; SPDX-FileCopyrightText: 2022 Emma Turner <em.turner@tutanota.com>
;
; SPDX-License-Identifier: MIT

(define-module (wlr render wlr-renderer)
  #:use-module (system foreign)
  #:use-module (system foreign-library)
  #:use-module (ice-9 format)
  #:use-module (wayland server-core)
  #:use-module (wlr dylib)
  #:use-module (wlr backend)
  #:use-module (emturner util)
  #:export-syntax (unwrap-wlr-renderer))

;; ------------------------------------
;; Wrappers for 'wlr/render/wlr_renderer.h'
;; WARNING: use of unstable api
;; ------------------------------------
(define-wrapped-pointer-type wlr-renderer
  wlr-renderer?
  wrap-wlr-renderer unwrap-wlr-renderer
  (lambda (wlr-r prt)
    (format prt "#<wlr-renderer at ~x>"
        (pointer-address (unwrap-wlr-renderer wlr-r)))))

(define wlr-renderer-auto-create
  (let ((autocreate
     (foreign-library-function wlroots "wlr_renderer_autocreate"
                   #:return-type '*
                   #:arg-types '(*))))
    (lambda (backend)
      "Automatically initializes the renderer"
      (wrap-wlr-renderer
       (autocreate (unwrap-wlr-backend backend))))))

(define wlr-renderer-init-wl-display
  (let ((init-display
     (foreign-library-function wlroots "wlr_renderer_init_wl_display"
                   #:return-type cstdbool
                   #:arg-types '(* *))))
    (lambda (renderer display)
      "Creates necessary shm and invokes the initialization of the
implementation.

Returns #f on failure."
      (let ((r (unwrap-wlr-renderer renderer))
        (d (unwrap-wl-display display)))
    (cstdbool->bool
     (init-display r d))))))

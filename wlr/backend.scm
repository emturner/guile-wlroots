; SPDX-FileCopyrightText: 2022 Emma Turner <em.turner@tutanota.com>
;
; SPDX-License-Identifier: MIT

(define-module (wlr backend)
  #:use-module (system foreign)
  #:use-module (system foreign-library)
  #:use-module (rnrs enums)
  #:use-module (srfi srfi-9)
  #:use-module (ice-9 format)
  #:use-module (ice-9 match)
  #:use-module (oop goops)
  #:use-module (wayland server-core)
  #:use-module (wlr dylib)
  #:use-module (wlr util log)
  #:use-module (wayland util)
  #:use-module (wlr types wlr-output)
  #:use-module (emturner util)
  #:use-module (emturner clock)
  #:use-module (tinywl wrapper)
  #:use-module (ice-9 optargs)
  #:export (wlr-backend-auto-create)
  #:export-syntax (unwrap-wlr-backend))


;; ----------------------------
;; Wrappers for 'wlr/backend.h'
;; WARNING: use of unstable api
;; ----------------------------

(define-wrapped-pointer-type wlr-backend
  wlr-backend?
  wrap-wlr-backend unwrap-wlr-backend
  (lambda (wlr-b prt)
    (format prt "#<wlr-backend at ~x>"
        (pointer-address (unwrap-wlr-backend wlr-b)))))

(define wlr-backend-auto-create
  (let ((autocreate
     (foreign-library-function wlroots "wlr_backend_autocreate"
                   #:return-type '*
                   #:arg-types '(*))))
    (lambda (display)
      "Automatically initializes the most suitable backend given the
environment. The backend is created but not started. Returns NULL on failure."
      (let ((backend (autocreate
              (unwrap-wl-display display))))
    (if (null-pointer? backend)
        (throw "wlr_backend_autocreate returned null")
        (wrap-wlr-backend backend))))))

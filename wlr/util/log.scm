; SPDX-FileCopyrightText: 2022 Emma Turner <em.turner@tutanota.com>
;
; SPDX-License-Identifier: MIT

(define-module (wlr util log)
  #:use-module (system foreign)
  #:use-module (system foreign-library)
  #:use-module (rnrs enums)
  #:use-module (srfi srfi-9)
  #:use-module (wlr dylib)
  #:export (wlr-log-init))
;; -----------------------------
;; Wrappers for 'wlr/util/log.h'
;; -----------------------------

(define wlr-log-importance
  (make-enumeration '(wlr-log-silent
                      wlr-log-error
                      wlr-log-info
                      wlr-log-debug
                      wlr-log-log-importance-last)))

;; TODO: Passes NULL callback - should be able to set this
(define (wlr-log-init verbosity)
  "Will log all messages less than or equal to `verbosity`"
  (let ((init (foreign-library-function wlroots "wlr_log_init"
                    #:return-type void
                    #:arg-types `(,int *)))
    (wlr-log-level ((enum-set-indexer wlr-log-importance)
                    verbosity)))
    (if wlr-log-level
        (init wlr-log-level %null-pointer))))

<!--
SPDX-FileCopyrightText: 2022 Emma Turner <em.turner@tutanota.com>
SPDX-License-Identifier: MIT
-->

# Guile Wlroots

# Running
The [manifest.scm](./manifest.scm) & [channels.scm](./channels.scm) files can be used to build & run.
``` sh
# Passthrough ENV vars to allow connection to libseat & logind
guix time-machine --channels=channels.scm -- shell --manifest=manifest.scm --pure -E '.*'

# Build & run
CC=gcc make check
```
